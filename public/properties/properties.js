
var properties = {
    //Version data
    version: "1.0",
    release: "GW-15.02.01",

    //Colors
    primary: "#8cc320",
    secondary: "#003348",
    gray: "#dfdee3",
    grayLight: "#f8f7f2",
    transparent: '#eae6e6ab',
    red: 'red',
    white: 'white',
    green: '#48E120',
    blue: 'blue',

    //Testing key
    testing: "0" // 0 = Apagado , 1 = Encendido


} 

// var data = CryptoJS.AES.encrypt(JSON.stringify(properties), Prdata).toString();
localStorage.setItem("properties", JSON.stringify(properties));