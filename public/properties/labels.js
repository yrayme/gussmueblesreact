
var properties = {
    //Buttoms
    ACCEPT_BUTTOM: "ACEPTAR",
    REGISTER_BUTTOM: "REGISTRAR",
    LOG_IN_BUTTOM: "INGRESAR",
    NEXT_BUTTOM: "CONTINUAR",
    PROCCES_BUTTOM: "PROCESAR",
    SEND_BUTTOM: "ENVIAR",
    LOGIN_BUTTOM: "INICIAR SESIÓN",
    CHECK_IN_BUTTOM: "Registrarse",


    //Labels
    LIKE_LB: "Me GUSSta",
    SAVE_LB: "Guardar para más tarde",
    EMAIL_LB: "Correo",
    NAME_LASTNAME_LB: "Nombre y Apellido",
    COMMENT_LB: "Comentario",
    EGUSS_LABEL: "E-GUSS",
    MEGUSSTA_LABEL: "Me GUSSta",
    WELCOME_LB: "¡Bienvenido!",
    USER_LB: "Correo Electrónico",
    PASSWORD_LB: "Contraseña",
    NAME_LB: "Nombre",
    LASTNAME_LB:"Apellido",
    BUSINESSNAME_LB:"Razón Social",
    MAIL_LB: "Correo",
    CONFIRMPASSWORD_LB: "Confirmar contraseña",
    CHECKBOX_LB: "Acepto las políticas de privacidad.",
    PUBLICITY_LB: "Recibir publicidad por correo.",
    IFORGET_LB: "Olvidó su contraseña"



} 

localStorage.setItem("labels", JSON.stringify(properties));