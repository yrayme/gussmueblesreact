import { createMuiTheme } from '@material-ui/core/styles';
import { DencryptData, EncryptData } from '../Tools/CryptoLocalStorage';
import MUEBLE from '../Components/Home/img/mueble.png';
import SILLA2 from '../Components/Home/img/silla2.png';
import SILLON from '../Components/Home/img/sillon.png';
import SOFA from '../Components/Home/img/sofa.png';
import MESA from '../Components/Home/img/mesa.png';

//LocalStorage encripts
export const SECRET_KEY = 'PrAEQUOUm9E3_CRiKMg';
export const PR_SECRET_KEY = 'PrAEQUOUm9E3';
export const ENCRYP_TYPE = {
  STRING: 'String',
  JSON: 'JSON',
  SECRET_KEY: 'KMGEncoded'
}
export const BASE = 'AEQUOUm9E3';
export const PROPERTIES_LS = 'Pr'.concat(BASE);
export const CREDIT_CARD_LS = 'cC'.concat(BASE);




export var properties = {};
try {
  if (localStorage.getItem("properties")) {
    properties = JSON.parse(localStorage.getItem("properties"));
    EncryptData(properties, ENCRYP_TYPE.JSON, PROPERTIES_LS)
    localStorage.removeItem("properties");
  } else {
    properties = DencryptData(ENCRYP_TYPE.JSON, PROPERTIES_LS);
  }
} catch (error) { }

/* Digitel Feature constants */
export const THEME = createMuiTheme({
  palette: {
    primary: {
      main: properties.primary,
    },
    secondary: {
      main: properties.secondary,
    }
  },

});

export const product = [
  {
    img: MUEBLE,
    title: 'Silla tentación',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: '5.350.000,00'
  },
  {
    img: SILLON,
    title: 'Mueble',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: SILLA2,
    title: 'Silla',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: MESA,
    title: 'Silla tentación',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: SOFA,
    title: 'Sofá Azul',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: MUEBLE,
    title: 'Silla',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: SILLON,
    title: 'Silla tentación',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: MESA,
    title: 'Mesa',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: SOFA,
    title: 'Silla',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: MUEBLE,
    title: 'Silla tentación',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: MESA,
    title: 'Mesa',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: SOFA,
    title: 'Sofá tentación',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  }, {
    img: MUEBLE,
    title: 'Silla tentación',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: '5.450.000,00'
  },
  {
    img: MESA,
    title: 'Mesa',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: SOFA,
    title: 'Sofa tentación',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: MUEBLE,
    title: 'Silla tentación',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: MESA,
    title: 'Mesa',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: SOFA,
    title: 'Silla',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: MUEBLE,
    title: 'Silla tentación',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  {
    img: MESA,
    title: 'Mesa',
    subtitle: 'Silla tentación',
    price: '5.450.000,00',
    subprice: null
  },
  // {
  //   img: SOFA,
  //   title: 'Silla',
  //   subtitle: 'Silla tentación',
  //   price: '5.450.000,00',
  //   subprice: null
  // },
  // {
  //   img: MUEBLE,
  //   title: 'Silla tentación',
  //   subtitle: 'Silla tentación',
  //   price: '5.450.000,00',
  //   subprice: null
  // },
  // {
  //   img: MESA,
  //   title: 'Mesa',
  //   subtitle: 'Silla tentación',
  //   price: '5.450.000,00',
  //   subprice: null
  // },
  // {
  //   img: SOFA,
  //   title: 'Silla',
  //   subtitle: 'Silla tentación',
  //   price: '5.450.000,00',
  //   subprice: null
  // },
]


