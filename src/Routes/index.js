import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import { ThemeProvider } from '@material-ui/styles';
import Home from '../Components/Home';
import Products from '../Components/Products';
import Profile from '../Components/Profile';
import ContactUs from '../Components/Footer/ContactUs';
import ShoppingCar from '../Components/ShoppingCar';
import Gussta from '../Components/Gussta';
import Login from '../Components/Home/Login';
import Register from '../Components/Home/Register'

import { THEME } from '../Constants';
const theme = THEME;

class Routes extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <ThemeProvider theme={theme}>
                        <Route exact path="/">
                            <Home />
                        </Route>
                        <Route path="/search">
                            <Products />
                        </Route>
                        <Route path="/profile">
                            <Profile />
                        </Route>
                        <Route path="/contact">
                            <ContactUs />
                        </Route>
                        <Route path="/shoppingcar">
                            <ShoppingCar />
                        </Route>
                        <Route path="/gussta">
                            <Gussta />
                        </Route>

                        <Route path="/login">
                            <Login />
                        </Route>

                        <Route path="/registro">
                            <Register />
                        </Route>
                    </ThemeProvider>
                </Switch>
            </Router >
        );
    }
}

export default Routes;