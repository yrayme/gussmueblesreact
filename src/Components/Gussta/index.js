import React, { Component } from 'react';
import { connect } from "react-redux";
import { Grid, withStyles } from '@material-ui/core';
import Header from '../Header';
import Footer from '../Footer';
import JUMBOTRON from './img/jumbotron.png';
import Paper from '@material-ui/core/Paper';
import PersonOutlineOutlinedIcon from '@material-ui/icons/PersonOutlineOutlined';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
// import Box from '@material-ui/core/Box';
import ButtonBase from '@material-ui/core/ButtonBase';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ListItem from '@material-ui/core/ListItem';


/**CONTANTS */
import { styles } from "./constants";

/** ACTIONS */
import { setHeaderProps } from '../../Redux/Actions/_actionHeader';

class Gussta extends Component {

    componentDidMount = () => {
        this.props.setHeaderProps('transparent', 'black');
    }

    render() {
        const { classes } = this.props;

        return (
            <>
                <Header />
                <Grid container direction="row">
                    <Grid item xs={12}>
                        <img
                            className="d-block w-100"
                            src={JUMBOTRON}
                            alt="First slide"
                            style={{ height: "60vh", objectFit: 'cover', }}
                        />
                    </Grid>
                    <Grid item xs={9}>
                        <div className={classes.paperJumbo}>
                            <Paper elevation={3}>
                                <Grid container direction="row">
                                    <Grid container xs={12} sm={4} md={3} lg={3} xl={3} >
                                        <PersonOutlineOutlinedIcon className={classes.personJumbo} />
                                    </Grid>
                                    <Grid container xs={12} sm={8} md={9} lg={9} xl={9} >
                                        <Grid container direction="column" justify="center" alignItems="baseline" className={classes.typography}>
                                            <Grid item>
                                                <Typography variant="h4" component="h3">
                                                    David Viloria
                                                </Typography>
                                            </Grid>
                                            <Grid item >
                                                <Typography gutterBottom>
                                                    David.viloria@kmg.com.ve
                                                </Typography>
                                            </Grid>

                                        </Grid>
                                        <Grid container direction="row" justify="space-around" alignItems="center" className={classes.typography} >
                                            <Grid item className={classes.iconsData}>
                                                <FavoriteBorderIcon className={classes.favoriteIconOutline} />
                                                <Typography gutterBottom>
                                                    95
                                                </Typography>
                                            </Grid>
                                            <Divider orientation="vertical" flexItem className={classes.divider} />
                                            <Grid item className={classes.iconsData}>
                                                <SaveOutlinedIcon className={classes.saveIcon} />
                                                <Typography gutterBottom>
                                                    15
                                                </Typography>
                                            </Grid>
                                            <Divider orientation="vertical" flexItem className={classes.divider} />
                                            <Grid item className={classes.iconsData}>
                                                <ShoppingCartOutlinedIcon className={classes.shoppingCarIcon} />
                                                <Typography gutterBottom>
                                                    10
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Paper>
                        </div>
                    </Grid>

                    {/* SIDEBAR */}
                    <Grid item xs={0} sm={3} md={3} lg={3} xl={3} className={classes.sidebar}>
                        <ListItem button>
                            <Typography className={classes.sideOptions}>
                                <strong>Compras</strong>
                            </Typography>
                        </ListItem>
                        <ListItem button>
                            <Typography className={classes.sideOptions}>
                                <strong>Cotizaciones</strong>
                            </Typography>
                        </ListItem>
                        <ListItem button>
                            <Typography className={classes.sideOptions}>
                                <strong>Me gussta</strong>
                            </Typography>
                        </ListItem>
                        <ListItem button>
                            <Typography className={classes.sideOptions}>
                                <strong>Guardados</strong>
                            </Typography>
                        </ListItem>
                        <ListItem button>
                            <Typography className={classes.sideOptions}>
                                <strong>Cambiar contraseña</strong>
                            </Typography>
                        </ListItem>
                        <ListItem button>
                            <Typography className={classes.sideOptions}>
                                <strong>Preguntas</strong>
                            </Typography>
                        </ListItem>
                        <ListItem button>
                            <Typography className={classes.sideOptions}>
                                <strong>Cerrar sesión</strong>
                            </Typography>
                        </ListItem>
                    </Grid>
                    {/* SIDEBAR */}

                    {/* MAIN */}
                    <Grid item xs={12} sm={9} md={9} lg={9} xl={9} className={classes.main}>
                        <Grid container direction='column' xs={10} alignItems="stretch" justify="space-around" >
                            <Grid item>
                                <Typography variant="h6" component="h3" className={classes.title}>
                                    <strong>Me GUSSta</strong>
                                </Typography>
                            </Grid>
                            <Grid item>
                                <div className={classes.mainData}>
                                    {/* MAIN CARDS */}
                                    <Grid container direction="row" spaing={6} justify="center">

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                                            <div className={classes.root}>
                                                <Paper className={classes.cardsMain}>
                                                    <Grid container spacing={2}>
                                                        <Grid item xs={6} style={{ background: '#f2f2f2', }}>
                                                            <ButtonBase className={classes.image}>
                                                                <img className={classes.img} alt="complex" src={window.location.origin + '/img/mueble.png'} />
                                                            </ButtonBase>
                                                        </Grid>
                                                        <Grid item xs={6} sm container>
                                                            <Grid item xs container direction="column" spacing={2} justify="space-evenly" alignItems="flex-end">
                                                                <Grid item >
                                                                    <Typography gutterBottom variant="subtitle1" style={{ color: '#8cc320' }}>
                                                                        <strong>
                                                                            Mueble relax
                                                                    </strong>
                                                                    </Typography>
                                                                    <Typography variant="subtitle1" style={{ color: '#000000' }}>Bs.5.450.000,00</Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                                        <FavoriteIcon className={classes.favoriteIcon} />
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </div>
                                        </Grid>

                                    </Grid>

                                    {/* MAIN CARDS */}
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                    {/* MAIN */}
                </Grid>

                <div style={{ marginTop: 150 }} >
                    <Footer />
                </div>

            </>
        );
    }
}

const gussta = withStyles(styles, { withTheme: true })(Gussta);

function mapStateToProps(state) {
    return {

    };
}

export default connect(mapStateToProps, {
    setHeaderProps,
})(gussta);