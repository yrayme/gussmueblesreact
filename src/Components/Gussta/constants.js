export const styles = (theme) => ({
    paperJumbo: {
        display: 'flex',
        flexWrap: 'wrap',
        '& > *': {
            width: theme.spacing(75),
            height: theme.spacing(24),
            marginTop: theme.spacing(-15),
            marginLeft: theme.spacing(7),
            background: "linear-gradient(to left, #0e3453 0%, #00aeed 100%)",
            borderRadius: 0
        },
    },
    personJumbo: {
        [theme.breakpoints.up('xs')]: {
            width: theme.spacing(0),
            height: theme.spacing(0),
        },
        [theme.breakpoints.up('sm')]: {
            color: 'white',
            width: theme.spacing(25),
            height: theme.spacing(25),
            marginTop: theme.spacing(-1),
            marginLeft: theme.spacing(-2),
        },
        [theme.breakpoints.up('md')]: {
            color: 'white',
            width: theme.spacing(25),
            height: theme.spacing(25),
            marginTop: theme.spacing(-1),
            marginLeft: theme.spacing(-2),
        },
        [theme.breakpoints.up('lg')]: {
            color: 'white',
            width: theme.spacing(25),
            height: theme.spacing(25),
            marginTop: theme.spacing(-1),
            marginLeft: theme.spacing(-2),
        },
    },
    typography: {
        color: 'white',
        [theme.breakpoints.up('xs')]: {
            margin: theme.spacing(2),
        },
    },
    divider: {
        background: 'white',
    },
    favoriteIcon: {
        width: theme.spacing(5),
        height: theme.spacing(5),
        color: '#ff0000',
        marginRight: theme.spacing(1),
    },
    favoriteIconOutline: {
        color: '#ff0000',
        marginRight: theme.spacing(1),
    },
    saveIcon: {
        color: '#1a3a91',
        marginRight: theme.spacing(1),
    },
    shoppingCarIcon: {
        color: '#7a975d',
        marginRight: theme.spacing(1),
    },
    iconsData: {
        display: 'flex',
    },
    title: {
        margin: theme.spacing(3, 0, 3, 7),
        color: '#013348'
    },
    sidebar: {
        [theme.breakpoints.up('xs')]: {
            // background: '#c0c0c0c4',
            // height: '100vh',
            height: theme.spacing(100),
        },
        [theme.breakpoints.up('sm')]: {
            background: '#c0c0c0c4',
            height: theme.spacing(100),
        },
        [theme.breakpoints.up('md')]: {
            background: '#c0c0c0c4',
            height: theme.spacing(100),
        },
        [theme.breakpoints.up('lg')]: {
            background: '#c0c0c0c4',
            height: theme.spacing(100),
        },
    },
    sideOptions: {
        [theme.breakpoints.up('xs')]: {
            margin: theme.spacing(0),
            fontSize: '0%',
        },
        [theme.breakpoints.up('sm')]: {
            margin: theme.spacing(1, 0, 1, 3),
            fontSize: '130%',
        },
        [theme.breakpoints.up('md')]: {
            margin: theme.spacing(1, 0, 1, 3),
            fontSize: '130%',
        },
        [theme.breakpoints.up('lg')]: {
            margin: theme.spacing(1, 0, 1, 3),
            fontSize: '130%',
        },
    },
    main: {
        margin: theme.spacing(-90, 0, 0, 0),
    },
    mainData: {
        height: theme.spacing(80),
        width: '120%',
        overflow: 'auto',
        background: '#dfdee3',

        '&::-webkit-scrollbar': {
            padding: '0.4em',
            margin: '0.4em',
            width: '0.4em',
            background: '#dfdee3',

        },
        '&::-webkit-scrollbar-track': {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
            webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',

        },
        '&::-webkit-scrollbar-thumb': {
            backgroundColor: '#aadd11',
            // outline: '1px solid slategrey',
            background: '#aadd11',

        }
    },

    root: {
        // flexGrow: 1,
        margin: theme.spacing(3, 2, 3, 2),
    },
    cardsMain: {
        margin: 'auto',
        maxWidth: 400,
        borderRadius: 0
    },
    image: {
        padding: theme.spacing(1),
        width: '90%',
        height: 128,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },

});
