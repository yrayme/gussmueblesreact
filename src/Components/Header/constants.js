export const useStyles = {
    root: {
        flexGrow: 1,
    },
    title: {
        flexGrow: 1,
    },
};