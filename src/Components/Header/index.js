import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import { useStyles } from './constants';

/*Actions*/
import { setHeaderProps } from '../../Redux/Actions/_actionHeader';
import { openProduct } from '../../Redux/Actions/_actionProducts';


class Header extends Component {

    handleHome = () => {
        this.props.openProduct(false);
        this.props.setHeaderProps('transparent', 'white');
    }

    render() {
        return (
            <AppBar position="absolute" style={{ backgroundColor: this.props.headerColor, boxShadow: "none" }}>
                <Toolbar>
                    <Button style={{ width: 120 }} color="inherit" onClick={this.handleHome}>
                        <i style={{ color: this.props.iconsColor, fontSize: 20, marginLeft: -60 }} class="icon-guss_logo" />
                    </Button>

                    <Typography variant="h6" style={useStyles.title}></Typography>

                    <Button color="inherit"><i style={{ color: this.props.iconsColor, fontSize: 22 }} class="icon-guss_user"></i></Button>
                    <Button color="inherit"><i style={{ color: this.props.iconsColor, fontSize: 22 }} class="icon-guss_shop"></i></Button>
                </Toolbar>
            </AppBar>
        );
    }
}



function mapStateToProps(state) {
    return {
        iconsColor: state.headerProps.iconsColor,
        headerColor: state.headerProps.headerColor

    };
}

export default connect(
    mapStateToProps, {
    setHeaderProps,
    openProduct
})(Header);