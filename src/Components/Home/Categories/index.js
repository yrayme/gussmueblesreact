import React, { Component } from 'react';
import { Grid, Button } from '@material-ui/core';
import Grow from '@material-ui/core/Grow';
import Search from '../Search';
import ArrowBackIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIcon from '@material-ui/icons/ArrowForwardIos';
import MUEBLE from '../img/cat 1-01.png';
import ARTU from '../img/cat 1-02.png';
import SILLA from '../img/cat 1-03.png';

import { properties } from '../../../Constants';


var categories = [
    {
        url: MUEBLE
    },
    {
        url: ARTU
    },
    {
        url: SILLA
    },
    {
        url: MUEBLE
    },
    {
        url: ARTU
    },
    {
        url: SILLA
    },

]
var list2 = [];


class Categories extends Component {

    constructor(props) {
        super(props);
        this.state = {
            animation: true,
            activeStep: 0,

        }
    }

    handleNext = () => {

        this.setState({
            animation: false,
            anima: true,
            direction: 'left',
        })

        setTimeout(() => {
            this.setState({

                animation: true,
                anima: false,
                activeStep: this.state.activeStep + 1,
            })
        }, 300);
    };

    handleBack = () => {
        this.setState({
            animation: false,
            anima: true,
            direction: 'right',
        })

        setTimeout(() => {
            this.setState({

                animation: true,
                anima: false,
                activeStep: this.state.activeStep - 1
            })
        }, 300);
    };

    render() {

        if (window.innerWidth > 959) {
            while (categories.length > 0) {
                list2.push(categories.splice(0, 4))
            }
        } else if (window.innerWidth < 959 && window.innerWidth > 500) {
            while (categories.length > 0) {
                list2.push(categories.splice(0, 2))
            }
        } else if (window.innerWidth <= 500) {
            while (categories.length > 0) {
                list2.push(categories.splice(0, 1))
            }
        }


        return (
            <Grid container spacing={0} style={{ marginTop: -160, position: "absolute" }}>
                <Grid item xs={1} style={{ textAlign: "right", paddingLeft: 20 }}>
                    <Button disabled={this.state.activeStep === 0}>
                        <ArrowBackIcon style={{ color: properties.secondary, marginTop: 210, marginBottom: 200, fontSize: 50 }} onClick={this.handleBack} />
                    </Button>

                </Grid>

                <Grid item xs={10} style={{ backgroundColor: "#eae6e6ab" }}>
                    <Grid container>

                        <Grid item xs={12} style={{ height: 90 }}>
                            <Grid container>
                                <Grid item xs={1}>
                                </Grid>
                                <Grid item xs={10} style={{ paddingLeft: 30, paddingRight: 30 }}>
                                    <Search />
                                </Grid>
                                <Grid item xs={1}>
                                </Grid>

                            </Grid>
                        </Grid>

                        <Grid item xs={12} style={{ height: 350, paddingTop: 0 }}>
                            <Grow in={this.state.animation}>
                                <Grid container style={{ paddingLeft: 20, paddingRight: 20 }} spacing={0}>

                                    {list2[this.state.activeStep].map(cat => (
                                        <Grid item xs={3} style={{ height: 200 }}>
                                            <img alt="mueble" src={cat.url} width="300" />
                                        </Grid>
                                    ))}

                                </Grid>
                            </Grow>
                        </Grid>


                    </Grid>

                </Grid>

                <Grid item xs={1} style={{ textAlign: "left" }}>
                    <Button disabled={this.state.activeStep === (list2.length - 1)}>
                        <ArrowForwardIcon  style={{ color: properties.secondary, marginTop: 210, marginBottom: 200, fontSize: 50 }} onClick={this.handleNext} />
                    </Button>
                </Grid>

            </Grid>
        );
    }
}

export default Categories;