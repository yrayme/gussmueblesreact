import React, { Component } from "react";
import { connect } from "react-redux";
import { Checkbox, FormControlLabel, withStyles } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Button from "@material-ui/core/Button";
import Slide from "@material-ui/core/Slide";
import TextField from "@material-ui/core/TextField";
import { Divider, Grid, Typography } from "@material-ui/core";
import LOGO from "../../Home/img/logo.png";
import { LOGINJSON, REGISTERJSON } from "./constants";

/*ACCIONES*/

/*CONSTANTES*/
import { properties } from "../../../Constants";
import { styles } from "./constants";

const labels = JSON.parse(localStorage.getItem("labels"));

class login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      json: LOGINJSON,
      check: false,
    };
  }

  /*FUNCIONES*/

  handleCheck = () => {
    this.setState({ check: !this.state.check });
  };

  onChangeValues = (event) => {
    var value = event.target.value;
    var state = event.target.name;
    this.setState({ [state]: value });
    //STATE ????
  };

  handleChange = (event, newValue) => {
    this.setState({ value: newValue });
  };

  handleSubmit = (event, pointer) => {
    switch (pointer) {
      case "1": //  BOTON "INGRESAR" JSON LOGIN
        break;
      case "2": // BOTON "REGISTRAR" JSON REGISTER
        break;
      case "3": // BOTON "INICIO DE SESION" PARA PINTAR FORMULARIO LOGIN
        break;
      case "4": // BOTON "REGISTRAR" PARA PUNTAR FORMULARIO REGISTRAR
        break;

      default:
        break;
    }
  };

  handleRegister = () => {
    this.setState({ json: REGISTERJSON });
  };

  handleLogin = () => {
    this.setState({ json: LOGINJSON });
  };

  //RENDERIZADO//
  render() {
    const { classes } = this.props;

    return (
      <Slide direction="left"  timeout={500} in={true} mountOnEnter unmountOnExit  >
        {this.state.json && (
          <div className={classes.root}  style={{ position: "absolute", top: 55, right: 0 }} >
            <Grid item xs={12} sm={12} md={12} lg={12}>

            <Tabs
              orientation="vertical"
              variant="scrollable"
              value={this.state.value}
              onChange={this.handleChange}
              aria-label="Vertical tabs example"
              style={this.state.json.tabs}
            >
              <Tab label={labels.LOGIN_BUTTOM} className={classes.transform} onClick={() => this.handleLogin()} />
              <Tab label={labels.CHECK_IN_BUTTOM} className={classes.transform} onClick={() => this.handleRegister()}/>
            </Tabs>
            </Grid>
            

            <Grid item xs={12} sm={12} md={12} lg={12}  >

            <Paper elevation={3} style={this.state.json.paper}>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <h5 className={classes.title}> {labels.WELCOME_LB} </h5>
                <Divider className={classes.divider} />
              </Grid>

              <Grid item xs={12} sm={12} md={12} lg={12}>
                <img
                  style={this.state.json.logo}
                  width={90}
                  alt="logo"
                  src={LOGO}
                />
              </Grid>

              <div style={this.state.json.formStyle}>
                <form id="publicForm">
                  <Grid container spacing={1}>
                    {this.state.json.inputs.map((element) => (
                      <Grid item xs={12} sm={12} md={12} lg={12}>
                        <TextField
                          label={element.label}
                          id={element.name}
                          name={element.name}
                          type={element.type}
                          onChange={this.onChangeValues}
                          className={classes.textColor}
                        />
                      </Grid>
                    ))}
                  </Grid>
                </form>
              </div>


              <Grid container spacing={1} className={classes.check}   >
              {this.state.json.id === 2 && (
                <Grid item xs={12} sm={12} md={12} lg={12}>

                  {this.state.json.headerLinks.map((element) => (
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <FormControlLabel
                        name="check"
                        control={
                           <Checkbox
                            checked={this.state.check}
                            onChange={this.handleChange}
                            name="check"
                            style={{ color: "white"}}
                          />
                        }
                        label={ 
                      <Typography variant="body2" style={{ fontSize: "0.75rem" }}>
                        {element.label} <strong  style={ { color:`${properties.primary}`, } }>  {element.link}</strong>
                      </Typography>
                      }
                      />

                   <FormControlLabel
                    name="check"
                    control={
                      <Checkbox
                        checked={this.state.check}
                        onChange={this.handleChange}
                        name="check"
                        style={{ color: "white"}}
                       
                      />
                    }
                    label={
                      <Typography variant="body2" style={{ fontSize: "0.75rem" }} >
                        <strong > ¿Quieres recibir nuestra publicidad?</strong>
                      </Typography>
                      
                    }
                  />
                    </Grid>
                  ))}
                
                </Grid>
              )}
                 

              </Grid>


              <Grid container>
                {this.state.json.button.map((element) => (
                  <Grid item xs={12} sm={12} md={12} lg={12}>
                    <div>
                      <Button
                        size="small"
                        variant="outlined"
                        color="primary"
                       
                        style={this.state.json.buttonStyle}
                      >
                        <strong className={classes.textButton}>
                          {element.name}
                        </strong>
                      </Button>
                    </div>
                  </Grid>
                ))}
              </Grid>
            </Paper>

            </Grid>

          </div>
        )}
      </Slide>
    );
  }
}

const Login = withStyles(styles, { withTheme: true })(login);

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, {

})(Login);

