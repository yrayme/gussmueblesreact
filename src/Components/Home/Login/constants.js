import { green } from "@material-ui/core/colors";
import { properties } from "../../../Constants";
const labels = JSON.parse(localStorage.getItem("labels"));

export const styles = (theme) => ({
  root: {
    flexGrow: 1,
    display: "flex",
    height: 224,
    float: "right",
  },
  transform: {
    transform: "rotate(-90deg)",
    marginTop: "15vh",
    marginLeft: -45,
    paddingTop: 15,
  },
  title: {
    color: properties.grayLight,
    position: "relative",
    left: 50,
    top: 30,
  },
  divider: {
    color: properties.grayLight,
    height: 1,
    marginLeft: 55,
    marginRight: 65,
    marginTop: "5vh",
  },
 
  textColor: {
    input: { color: "white" },
    "& label.Mui-focused": {
      color: "white",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "white",
      color: "white",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "white",
        color: "white",
      },
      "&:hover fieldset": {
        borderColor: "white",
        color: "white",
      },
      "&.Mui-focused fieldset": {
        borderColor: "white",
        color: "white",
      },
    },
  },

  check: {
    width: "80%",
    marginLeft: 20,
    marginTop: 10,

  },

  textButton: {
    padding: "3px 25px 3px 25px",
    fontSize: 10,
  },
});

///////////////////// JSON-INICIO DE SESIÓN //////////////////////////////

export const LOGINJSON = {
  id: 1, // 1= Login, 2= Registro
  formStyle:{
    width: "50%",
    marginLeft: 50,
    marginTop: 20,

  },
  buttonStyle: {
      
    borderRadius: "0px",
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    position: "relative",
    marginTop: 40,
    marginLeft: 40,
    width: "50%",
  },
  paper: {
    borderRadius: "0px",
    minHeight: "60vh",
    width: 300,
    background: "linear-gradient(to top, #013348 0%, #0585ba 100%)",
  },
  tabs: {
    borderRight: "1px solid ${theme.palette.divider}",
    minHeight: "60vh",
    width: 55,
    backgroundColor: properties.gray,
  },
  logo: {
    position: "relative",
    top: 300,
    marginLeft: 100,
  },
  inputs: [
    {
      label: `${labels.USER_LB}`,
      type: "email",
      name: "email",
      pattern: /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,9}$/i,
    },
    {
      label: `${labels.PASSWORD_LB}`,
      type: "password",
      name: "password",
      pattern: /^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[-+_!@#$%^&*.,?]).*$/,
    },
  ],
  button: [
    {
      pointer: "1",
      name: `${labels.LOG_IN_BUTTOM}`,
    },

  ],
  links: [
    {
      label: `${labels.IFORGET_LB}`,
    },
  ],
};

//////////////// JSON-REGISTRAR///////////

export const REGISTERJSON = {
  id: 2, // 1= Login, 2= Registro
  formStyle:{
    width: "70%",
    marginLeft: 30,
   

  },
  buttonStyle: {
    borderRadius: "0px ",
    position: "relative",
    marginTop: 10,
    marginLeft: 40,
    width: "50%",
  },
  paper: {
    borderRadius: "0px",
    minHeight: "90vh",
    width: 300,
    background: "linear-gradient(to top, #013348 0%, #0585ba 100%)",
  },
  tabs: {
    borderRight: "1px solid ${theme.palette.divider}",
    minHeight: "90vh",
    width: 55,
    backgroundColor: properties.gray,
  },
  logo: {
    position: "relative",
    top: 550,
    marginLeft: 100,
  },
  inputs: [
    {
      label: `${labels.NAME_LB}`,
      type: "text",
      name: "name",
      pattern: /^([^0-9-+_!/@#$%^&*.,?]*)$/,
    },
    {
      label: `${labels.LASTNAME_LB}`,
      type: "text",
      name: "surname",
      pattern: /^([^0-9-+_!/@#$%^&*.,?]*)$/,
    },
    {
      label: `${labels.BUSINESSNAME_LB}`,
      type: "text",
      name: "social",
    },
    {
      label: `${labels.MAIL_LB}`,
      type: "email",
      name: "email",
      pattern: /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,9}$/i,
    },
    {
      label: `${labels.PASSWORD_LB}`,
      type: "password",
      name: "password",
      pattern: /^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[-+_!@#$%^&*.,?]).*$/,
    },
    {
      label: `${labels.CONFIRMPASSWORD_LB}`,
      type: "password",
      name: "confirmPassword",
      pattern: /^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[-+_!@#$%^&*.,?]).*$/,
    },
  ],

  button: [
    {
      pointer: "3",
      name: `${labels.REGISTER_BUTTOM}`,
    },

  ],
  headerLinks:[
      {
          id: "1",
          label: " Acepto las", 
          link: "política de privacidad",
      }
  ]
};
