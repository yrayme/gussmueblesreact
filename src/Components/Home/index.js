import React, { Component } from 'react';
import { connect } from 'react-redux';

/*Components */
import CarouselData from './CarouselData';
import Header from '../Header';
import Categories from './Categories';
import Footer from '../Footer';
import Products from '../Products';
import ModalSearch from '../Products/ModalSearch';
// import ProductDetail from '../Products/ProductDetail';


/*Actions*/
import { setHeaderProps } from '../../Redux/Actions/_actionHeader';




class Home extends Component {

    componentDidMount = () => {
        this.props.setHeaderProps('transparent', 'white');
    }

    render() {
        return (
            <>
                <Header />
                {this.props.productOpen ?
                    <Products />
                    :
                    <>
                        <CarouselData />
                        <Categories />
                    </>
                }

                {this.props.searchModal && <ModalSearch />}

                <div style={{ marginTop: this.props.productOpen ? '0px' : '450px' }} >
                    <Footer />
                </div>

            </>
        );
    }
}


function mapStateToProps(state) {
    return {
        productOpen: state.products.productOpen,
        searchModal: state.products.searchModal,
        // productDetails: state.products.productDetails
    };
}

export default connect(
    mapStateToProps, {
    setHeaderProps
})(Home);