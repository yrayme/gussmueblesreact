import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import "bootstrap/dist/css/bootstrap.min.css"

import HOME from '../img/home.jpg';

class CarouselData extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            index: 0
        })
    }


    render() {
        return (
            <Carousel nextIcon="" prevIcon="" indicators={false}>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={HOME}
                        alt="First slide"
                        style={{height: "60vh", objectFit: 'cover',}}
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={HOME}
                        alt="Third slide"
                        style={{height: "60vh", objectFit: 'cover',}}
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={HOME}
                        alt="Third slide"
                        style={{height: "60vh", objectFit: 'cover',}}
                    />
                </Carousel.Item>
            </Carousel>
        );
    }
}

export default CarouselData;