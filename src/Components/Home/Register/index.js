import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Slide from '@material-ui/core/Slide';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import { properties} from '../../../Constants';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}




const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        height: 224,
        float: "right"
    },
    tabs: {
        borderRight: `1px solid ${theme.palette.divider}`,
    },
}));

export default function VerticalTabs() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Slide direction="left" timeout={500} in={true} mountOnEnter unmountOnExit>
            <div className={classes.root} style={{
                position: "absolute",
                top: 55,
                right: 0,
            }}>

                <Tabs
                    orientation="vertical"
                    variant="scrollable"
                    value={value}
                    onChange={handleChange}
                    aria-label="Vertical tabs example"
                    className={classes.tabs}
                    style={{ minHeight: "70vh", width: 55, backgroundColor: properties.gray }}
                >
                    <Tab label="Ingresar" {...a11yProps(0)} style={{ transform: "rotate(-90deg)", marginTop: "15vh", marginLeft: -45, paddingTop: 15 }} />
                    <Tab label="Registrarse" {...a11yProps(1)} style={{ transform: "rotate(-90deg)", marginTop: "15vh", marginLeft: -45, paddingTop: 15 }} />

                </Tabs>
                <Paper elevation={3} style={{ minHeight: "70vh", width: 300, background: "linear-gradient(to top, #013348 0%, #0585ba 100%)", }}>
                    <TabPanel value={value} index={0}>
                        Item One
                </TabPanel>
                    <TabPanel value={value} index={1}>
                        Item Two
                </TabPanel>

                </Paper>

            </div>
        </Slide>
    );
}







































































///////////////// REGISTRO ////////////////////////



// import React, { Component } from "react";
// import Typography from "@material-ui/core/Typography";
// import Button from "@material-ui/core/Button";
// import TextField from "@material-ui/core/TextField";
// import Grid from "@material-ui/core/Grid";
// import Divider from "@material-ui/core/Divider";
// import Paper from "@material-ui/core/Paper";
// import LOGO from "../../Home/img/logo.png";
// import TabPanel from "@material-ui/core/Tabs";
// import Checkbox from "@material-ui/core/Checkbox";
// import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
// import CheckBoxIcon from '@material-ui/icons/CheckBox';
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import { properties } from "../../../Constants";

// const labels = JSON.parse(localStorage.getItem("labels"));

// class Register extends Component {
//   render() {
//     return (
//       <div>
//         <Grid container spacing={1}>
//           <Grid
//             item
//             xs={12}
//             sm={12}
//             md={12}
//             lg={12}
//             style={{ paddingTop: 50, paddingLeft: 60 }}
//           >
//             <Paper
//               style={{
//                 borderRadius: 0,
//                 height: 650,
//                 width: 340,
//                 background:
//                   "linear-gradient(to bottom, #07b4ff 0%, #173644 100%)",
//               }}
//               elevation={2}
//             >
//               <Grid item xs={12} sm={12} md={12}  lg={12} style={{ textAlign: "center" }} >
//                 <TabPanel
//                   style={{ transform: "rotate(90deg)", transformOrigin: "left top", backgroundColor: properties.gray, width: 650,  }}  >
//                   <Grid item xs={12} sm={12} md={12} lg={12}>
//                     <Button
//                       style={{ position: "absolute", width: 40, top: 10, right: 450,   }} >
//                       <h6 style={{ color: properties.secondary }}>
//                         {labels.LOGIN_BUTTOM}
//                       </h6>
//                     </Button>
//                   </Grid>

//                   <Divider
//                     orientation="vertical"
//                     style={{ backgroundColor: "black", height: 30, margin: 10 }}
//                   />

//                   <Grid item xs={12} sm={12} md={12} lg={12}>
//                     <Button style={{   position: "absolute",   width: 70,   top: 10,  left: 450, }}>
//                       <h6 style={{ color: properties.secondary }}>
//                         {labels.CHECK_IN_BUTTOM}
//                       </h6>
//                     </Button>
//                   </Grid>
//                 </TabPanel>
//               </Grid>

//               <Grid container    spacing={1}     xs={12}   sm={12}  md={12}   lg={12}  style={{ textAlign: "center" }}>
//                 <Grid item xs={12} sm={12} md={12} lg={12}>
//                   <h5
//                     style={{
//                       color: properties.grayLight,
//                       position: "relative",
//                       right: 50,
//                     }}
//                   >
//                     {labels.WELCOME_LB}
//                   </h5>
//                   <Divider
//                     style={{
//                       color: properties.grayLight,
//                       height: 1,
//                       marginLeft: 55,
//                       marginRight: 65,
//                     }}
//                   />
//                 </Grid>

//                 <Grid item xs={12} sm={12} md={12} lg={12}>
//                   <TextField
//                     id="standard-basic"
//                     label={labels.NAME_LB}
//                     color="secondary"
//                   />
//                 </Grid>

//                 <Grid item xs={12} sm={12} md={12} lg={12}>
//                   <TextField
//                     id="standard-basic"
//                     label={labels.LASTNAME_LB}
//                     color="secondary"
//                   />
//                 </Grid>

//                 <Grid item xs={12} sm={12} md={12} lg={12}>
//                   <TextField
//                     id="standard-basic"
//                     label={labels.BUSINESSNAME_LB}
//                     color="secondary"
//                   />
//                 </Grid>

//                 <Grid item xs={12} sm={12} md={12} lg={12}>
//                   <TextField
//                     id="standard-basic"
//                     label={labels.MAIL_LB}
//                     color="secondary"
//                   />
//                 </Grid>

//                 <Grid item xs={12} sm={12} md={12} lg={12}>
//                   <TextField
//                     id="standard-basic"
//                     label={labels.PASSWORD_LB}
//                     color="secondary"
                    
//                   />
//                 </Grid>

//                 <Grid item xs={12} sm={12} md={12} lg={12}>
//                   <TextField
//                     id="standard-basic"
//                     label={labels.CONFIRMPASSWORD_LB}
//                     color="secondary"
//                   />
//                 </Grid>

//                 <Grid item xs={12} sm={12} md={12} lg={12}>
//                     <FormControlLabel 
//                     control={
//                         <Checkbox
//                         defaultChecked
//                         size="small"
//                         inputProps={{ 'aria-label': 'checkbox with small size' }}
//                         />
//                     }
//                     label= {labels.CHECKBOX_LB} 
//                     />
//                 </Grid>
               
//                 <Grid item xs={12} sm={12} md={12} lg={12}>
//                     <FormControlLabel 
//                     control={
//                         <Checkbox
//                         icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
//                         checkedIcon={<CheckBoxIcon fontSize="small" />}
//                         name="checkedI"
//                         />
//                     }
//                     label= {labels.PUBLICITY_LB} 
//                     />
//                 </Grid>

//                 <Grid item xs={12} sm={12} md={12} lg={12}>
//                   <Button
//                     style={{
//                       border: "1px solid #8cc320",
//                       position: "relative",
//                       right: 50,
//                     }}
//                   >
//                     <span style={{ color: properties.primary, fontSize: 12, }}>
//                     {labels.REGISTER_BUTTOM}
//                     </span>
//                   </Button>
//                 </Grid>
//               </Grid>

//               <Grid item xs={12} sm={12} md={12} lg={12}>
//                 <img
//                   style={{ position: "relative", top: 30, marginLeft: 120 }}
//                   width={90}
//                   alt="logo"
//                   src={LOGO}
//                 />
//               </Grid>
//             </Paper>
//           </Grid>
//         </Grid>
//       </div>
//     );
//   }
// }

// export default Register;
