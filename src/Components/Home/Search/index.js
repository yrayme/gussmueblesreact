import React from 'react';
import { useDispatch } from 'react-redux';
import { Paper, Grid, Divider, InputBase, IconButton, Select, MenuItem } from '@material-ui/core';

// import SearchIcon from '@material-ui/icons/Search';
import { useStyles } from './constants.js';

/*Actions */
import { openProduct } from '../../../Redux/Actions/_actionProducts';
import { setHeaderProps } from '../../../Redux/Actions/_actionHeader';

import '../../Products/index.css';



function CustomizedInputBase() {

    const dispatch = useDispatch();
    const classes = useStyles();

    const search = (event) => {
        event.preventDefault();
        dispatch(openProduct(true));
        dispatch(setHeaderProps('#24303e', 'white'));
    }

    return (
        <Grid container className={classes.marginTop}>
            <Grid item xs={12} className={classes.hiddenMobile}>
                <Paper component="form" className={classes.root} elevation={0}  >
                    {/* <form onSubmit={search}> */}
                    <Select
                        className={classes.select}
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        value={20}
                        label="Age"
                        variant="standard"
                        color="primary"
                        size="small"
                    >
                        <MenuItem value={50}>Escritorios</MenuItem>
                        <MenuItem value={10}>Arturito</MenuItem>
                        <MenuItem value={20}>Muebles</MenuItem>
                        <MenuItem value={30}>Sillas</MenuItem>
                    </Select>
                    <Divider className={classes.divider} orientation="vertical" />

                    <InputBase
                        className={classes.input}
                        placeholder="Búsqueda"
                        inputProps={{ 'aria-label': 'search google maps' }}
                    />
                    <IconButton color="secondary" type="submit" className={classes.iconButton} aria-label="search" onClick={search}>
                        <i style={{ fontSize: 20 }} class="icon-guss_search"></i>
                    </IconButton>
                    {/* </form> */}
                </Paper>
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={classes.hiddenDesktop}>
                <Grid container>
                    <Grid xs={12} sm={6} md={6}>
                        <Select
                            className={classes.selectMobile}
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined"
                            value={20}
                            label="Age"
                            variant="standard"
                            color="primary"
                        >
                            <MenuItem value={50}>Escritorios</MenuItem>
                            <MenuItem value={10}>Arturito</MenuItem>
                            <MenuItem value={20}>Muebles</MenuItem>
                            <MenuItem value={30}>Sillas</MenuItem>
                        </Select>
                    </Grid>

                    <Grid xs={12} sm={6} md={6}>
                        <Paper component="form" className={classes.rootMobile} elevation={0} variant="outlined" >
                            <InputBase
                                className={classes.input}
                                placeholder="Búsqueda"
                                inputProps={{ 'aria-label': 'search google maps' }}
                                color='secondary'
                                variant="outlined"
                            />
                            <IconButton color="secondary" type="submit" className={classes.iconButton} onClick={search} aria-label="search">
                                <i style={{ fontSize: 20 }} class="icon-guss_search"></i>
                            </IconButton>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>

        </Grid>
    );
}
export default CustomizedInputBase