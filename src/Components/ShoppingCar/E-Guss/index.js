import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid, Paper, withStyles, Typography } from "@material-ui/core";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import Link from '@material-ui/core/Link';

import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

//Componentes
import MUEBLE from "../MeGussta/img/mueble_azul.png";
import { styles } from "./constants.js";

//Iconos
import SaveOutlinedIcon from "@material-ui/icons/SaveOutlined";
import HighlightOffOutlinedIcon from '@material-ui/icons/HighlightOffOutlined';

class EGuss extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        {
          cantidad: 3,
          precio: '5450999',
        },
        {
          cantidad: 1,
          precio: '2234567',
        },
        {
          cantidad: 4,
          precio: '5123987',
        },
        {
          cantidad: 3,
          precio: '8000000',
        },
      ],
      sumatoriaTotal: 0
    };
  }

  componentDidMount(){
    this.sumatoriaPrecio();
  }


  deleteItem = (i) => {
    const { items } = this.state;
    items.splice(i, 1);
    this.setState({ items });
    this.sumatoriaPrecio();
  }

  calcularCantidad = (i, tipo) => {
    let items = [...this.state.items];
    let item = {...items[i]};
    let cantidadActual = this.state.items[i].cantidad;
    let calculo = 0;
    
    switch (tipo) {
      case 1:
        calculo = parseInt(cantidadActual) - 1; 

        if( cantidadActual <= 1) calculo = 0;
        break;
    
      default:
        calculo = parseInt(cantidadActual) + 1; 
        break;
    }
    
    
    item.cantidad = calculo;
    items[i] = item;
    this.setState({items});
    this.sumatoriaPrecio();
  }

  sumatoriaPrecio = () =>{
    let objeto = [...this.state.items];
    var total = 0;

    for (let i = 0; i < objeto.length; i++) {
      total = (objeto[i].cantidad * objeto[i].precio) + total;
    }
    console.log(total)
    this.setState({sumatoriaTotal: total})
  }

  render() {
    const { classes } = this.props;
    const formatter = new Intl.NumberFormat('es-ES', {
      style: 'currency',
      currency: 'VES',
      minimumFractionDigits: 2
    })
    return (
      <>
        <Paper elevation={2} square className={classes.paper}>
          <Grid container>
            {/* Tabla de artículos para el carrito */}
            <Grid xs={12} className={classes.tabla}>
              <Table size="small" aria-label="a dense table">
                <TableHead>
                  <TableRow>
                    <TableCell className={classes.headerTitulo}>Cantidad</TableCell>
                    <TableCell className={classes.headerTitulo}>Producto</TableCell>
                    <TableCell className={classes.headerTitulo}>Precio Unitario</TableCell>
                    <TableCell className={classes.headerTitulo}>Total</TableCell>
                    <TableCell className={classes.headerTitulo}></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.items.map((data, key) => (
                    <TableRow key={key}>
                      <TableCell component="th" scope="row" align="center">
                        <TextField type="number" variant="outlined" className={classes.textField} value={data.cantidad}/>
                        <div>
                          <Button variant="outlined" color="primary" className={classes.btnMinus} onClick={ ()=> this.calcularCantidad(key, 1) }>
                            -
                          </Button>
                          <Button variant="outlined" color="primary" className={classes.btnPlus} onClick={ ()=> this.calcularCantidad(key, 0) }>
                            +
                          </Button>
                        </div>
                      </TableCell>
                      <TableCell align="center">
                        <img alt="mueble" src={MUEBLE} width="120px" />
                      </TableCell>
                      <TableCell align="center">Bs.S {formatter.format(data.precio)}</TableCell>
                      <TableCell align="center">Bs.S {formatter.format(data.cantidad * data.precio)}</TableCell>
                      <TableCell align="center">
                        <SaveOutlinedIcon className={classes.icon} />
                        <HighlightOffOutlinedIcon className={classes.icon} onClick={ ()=> this.deleteItem(this, key) }/>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Grid>

            {/* Footer sección artículos de carrito */}
            <Grid xs={12} className={classes.footer}>
              <Grid container>
                <Grid item xs={10}>
                  <Typography variant="h6">
                    Total Bs. S {formatter.format(this.state.sumatoriaTotal)}
                  </Typography>
                  <Link href="#" color='secondary'>
                    Cotizar
                  </Link>
                </Grid>
                <Grid item xs={2}>
                  <Button variant="outlined" color="primary" className={classes.btnMinus}>
                    Procesar
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </>
    );
  }
}

const eGuss = withStyles(styles, { withTheme: true })(EGuss);

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, {})(eGuss);
