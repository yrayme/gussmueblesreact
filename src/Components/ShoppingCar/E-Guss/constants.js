import { properties } from "../../../Constants";

export const styles = (theme) => ({
  paper: {
    marginRight: theme.spacing(1.5),
  },
  tabla: {
    overflowY: "auto",
    height: "40vh !important",
    textAlign: "right",
  },
  footer: {
    height: "10vh !important",
    textAlign: "right",
    borderTop: "1px solid" + properties.gray,
    paddingTop: theme.spacing(1),
    "& > .MuiGrid-container": {
      paddingRight: theme.spacing(1),
    },
  },
  icon: {
    color: properties.secondary,
    fontSize: 40,
    marginRight: theme.spacing(1),
    marginTop: "-3px",
    "& :active": {
      color: properties.primary,
    },
  },
  headerTitulo: {
    fontWeight: "700",
    textAlign: "center",
    fontSize: "18px",
  },
  textField: {
    width: "145px",
    marginBottom: theme.spacing(1),
    borderRadius: "0px",
    "& .MuiOutlinedInput-root": {
        borderRadius: "0px",
        '& > .MuiInputBase-input':{
            textAlign: "center",
        }
    },
  },
  btnPlus: {
    marginLeft: theme.spacing(1),
    borderRadius: "0px",
  },
  btnMinus: {
    marginRight: theme.spacing(1),
    borderRadius: "0px",
  },
});
