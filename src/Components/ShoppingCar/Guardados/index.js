import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid, Typography, withStyles } from "@material-ui/core";

/* Iconos */
// import FavoriteIcon from "@material-ui/icons/Favorite";
// import FavoriteBorderOutlinedIcon from '@material-ui/icons/FavoriteBorderOutlined';
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";


import { styles } from "./constants.js";
import MUEBLE from "../MeGussta/img/mueble_azul.png";

class Guardados extends Component {
  render() {
    const { classes } = this.props;
    const objeto = [
      {
        nombre: "Mueble Relax",
        precio: "5.430.000",
      },
      {
        nombre: "Mueble Chicago",
        precio: "7.440.000",
      },
      {
        nombre: "Mueble dos puestos",
        precio: "9.440.000",
      },
      {
        nombre: "Mueble Relax",
        precio: "5.430.000",
      },
      {
        nombre: "Mueble Chicago",
        precio: "7.440.000",
      },
      {
        nombre: "Mueble dos puestos",
        precio: "9.440.000",
      }
    ];
    return (
      <>
        <Grid container spacing={0} className={classes.gridMain}>
          {objeto.map((data, key) => (
            <Grid key={key} item xs={12} sm={6} md={6} lg={4} xl={4} style={{padding: '8px'}}>
              <Grid container className={classes.contenedorArticulo}>
                <Grid item xs={5} className="img">
                  <img alt="mueble" src={MUEBLE} />
                </Grid>
                <Grid item xs={7} className='nombre'>
                  <Typography variant="h5">{data.nombre}</Typography>
                  <Typography variant="subtitle1">Bs.S {data.precio}</Typography>
                  <ShoppingCartOutlinedIcon className={classes.icon} />
                </Grid>
              </Grid>
            </Grid>
          ))}
        </Grid>
      </>
    );
  }
}

const guardados = withStyles(styles, { withTheme: true })(Guardados);

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, {})(guardados);
