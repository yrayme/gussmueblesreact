import { properties } from '../../../Constants';

export const styles = (theme) => ({
    gridMain: {
        height:"100%", 
        background: properties.gray,
        padding: theme.spacing(3)
    },
    icon: {
        color: properties.secondary, 
        fontSize: 40,
        marginTop: theme.spacing(1) 
    },
    contenedorArticulo:{
        textAlign: "right",
        height: '150px',
        background: properties.white,
        '& > .img, & > .img > img':{
            width: '100%',
            height: '100%'
        },
        '& > .nombre':{
            padding: theme.spacing(2)
        },
        '& > div > h5':{
            color: properties.primary,
            fontWeight: "600",
            fontSize: '1.2rem'
        },
        '& > div > h6':{
            color: properties.secondary,
            fontWeight: "500"
        },
    },
});