import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid, Typography, withStyles } from "@material-ui/core";

import EGuss from "./E-Guss";
import MeGussta from "./MeGussta";
import Guardados from "./Guardados";
import Header from "../Header";
import Footer from "../Footer";

import { styles } from "./constants.js";

//Iconos
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";
import FavoriteIcon from "@material-ui/icons/Favorite";
import SaveOutlinedIcon from "@material-ui/icons/SaveOutlined";

class ShoppingCar extends Component {
  render() {
    const { classes } = this.props;
    return (
      <>
        {/* Header */}
        <div className={classes.header}>
          <Header />
        </div>
        

        <Grid container spacing={0}>
          {/* Sección E-Guss */}
          <Grid item xs={9}>
            <Typography variant="h5" className={classes.titulo}>
              <ShoppingCartOutlinedIcon className={classes.icon} />
              E-Guss
            </Typography>
            <EGuss />
          </Grid>


          {/* Sección Me Gussta */}
          <Grid item xs={3}>
            <Typography variant="h5" className={classes.titulo}>
              <FavoriteIcon className={classes.icon} />
              Me Gussta
            </Typography>
            <MeGussta />
          </Grid>


          {/* Sección Guardados */}
          <Grid item xs={12}>
            <Typography variant="h5" className={classes.titulo}>
              <SaveOutlinedIcon className={classes.icon} />
              Guardado para más tarde
            </Typography>
            <Guardados />
          </Grid>
        </Grid>


        {/* Footer */}
        <div style={{ marginTop: 200 }}>
          <Footer />
        </div>
      </>
    );
  }
}


const shoppingCar = withStyles(styles, { withTheme: true })(ShoppingCar);

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, {})(shoppingCar);