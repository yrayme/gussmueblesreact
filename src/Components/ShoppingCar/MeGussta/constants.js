import { properties } from "../../../Constants";

export const styles = (theme) => ({
  "@global": {
    "*::-webkit-scrollbar": {
      width: "0.4em",
    },
    "*::-webkit-scrollbar-track": {
      "-webkit-box-shadow": "inset 0 0 6px rgba(0,0,0,0.00)",
    },
    "*::-webkit-scrollbar-thumb": {
      backgroundColor: "rgba(0,0,0,.1)",
      outline: "1px solid graylight",
    },
  },
  icon: {
    color: "#d40909",
    fontSize: 25,
    marginTop: theme.spacing(1),
  },
  paper: {
    height: "50vh",
    background: properties.gray,
    padding: theme.spacing(2),
    overflowY: "auto",
  },
  contenedorArticulo: {
    textAlign: "right",
    height: "120px",
    background: properties.white,
    margin: theme.spacing(1),
    "& > .img, & > .img > img": {
      width: "100%",
      height: "100%",
    },
    "& > .nombre": {
      padding: theme.spacing(2),
    },
    "& > div > h5": {
      color: properties.primary,
      fontWeight: "600",
      fontSize: "1rem",
    },
    "& > div > h6": {
      color: properties.secondary,
      fontWeight: "500",
    },
  },
});
