import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid, Typography, withStyles } from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FavoriteBorderOutlinedIcon from "@material-ui/icons/FavoriteBorderOutlined";
import { styles } from "./constants.js";
import MUEBLE from "./img/mueble_azul.png";

class MeGussta extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items : [
        {
          nombre: "Mueble Relax",
          precio: "5.430.000",
          like: 1,
        },
        {
          nombre: "Mueble Chicago",
          precio: "7.440.000",
          like: 0,
        },
        {
          nombre: "Mueble dos puestos",
          precio: "9.440.000",
          like: 1,
        },
        {
          nombre: "Mueble Relax",
          precio: "5.430.000",
          like: 0,
        },
        {
          nombre: "Mueble Chicago",
          precio: "7.440.000",
          like: 1,
        },
        {
          nombre: "Mueble dos puestos",
          precio: "9.440.000",
          like: 0,
        },
      ]
    }
  }


  clicked = (i) =>{
    let items = [...this.state.items];
    let item = {...items[i]};

    if(item.like){
      item.like = 0;
    }else{
      item.like = 1;
    }

    items[i] = item;
    this.setState({items});
  }
  
  render() {
    const { classes } = this.props;
    return (
      <>
        <Grid container className={classes.paper}>
          {this.state.items.map((data, key) => (
            <Grid key={key} item xs={12}>
              <Grid container className={classes.contenedorArticulo}>
                <Grid item xs={5} className="img">
                  <img alt="mueble" src={MUEBLE} />
                </Grid>
                <Grid item xs={7} className="nombre">
                  <Typography variant="h5">{data.nombre}</Typography>
                  <Typography variant="subtitle1">
                    Bs.S {data.precio}
                  </Typography>
                  {data.like ? (
                    <FavoriteIcon className={classes.icon} onClick={ () => this.clicked(key)}/>
                  ) : (
                    <FavoriteBorderOutlinedIcon className={classes.icon} onClick={ () => this.clicked(key)}/>
                  )}
                </Grid>
              </Grid>
            </Grid>
          ))}
        </Grid>
      </>
    );
  }
}

const megussta = withStyles(styles, { withTheme: true })(MeGussta);

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, {})(megussta);
