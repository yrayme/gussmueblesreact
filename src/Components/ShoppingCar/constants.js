import { properties } from '../../Constants';

export const styles = (theme) => ({
    header:{
        height: "10vh", 
        objectFit: "cover", 
        width: "100%", 
        background:'linear-gradient(to left, '+properties.secondary+' 0%, '+properties.gray+' 100%)'
    },
    titulo:{
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        marginLeft: theme.spacing(2)
    },
    icon:{
        color: properties.primary, 
        fontSize: 30,
        marginRight: theme.spacing(1),
        marginTop: '-3px',
    },
});