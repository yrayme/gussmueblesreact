
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Magnifier from "react-magnifier";
import {
    Slide, Dialog, Grid, Typography, IconButton, Divider, CardActionArea, Grow,
    Button, Chip, Paper, InputBase
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIcon from '@material-ui/icons/ArrowForwardIos';
import { withStyles } from '@material-ui/core';
import SOFA from '../../Home/img/sofa.png';

/*Actions */
import { detailProduct } from '../../../Redux/Actions/_actionProducts';

/*Components */

/*Constants */
import { styles, colors } from '../constants.js';
import { properties } from '../../../Constants';


const labels = JSON.parse(localStorage.getItem("labels"))

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

var categories = [
    {
        url: SOFA
    },
    {
        url: SOFA
    },
    {
        url: SOFA
    },
    {
        url: SOFA
    },
    {
        url: SOFA
    },
    {
        url: SOFA
    },

]

var product = [];

class ProductDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animation: true,
            activeStep: 0,
        }
    }

    handleClose = () => {
        this.props.detailProduct(false);
    }

    handleNext = () => {

        this.setState({
            animation: false,
            anima: true,
            direction: 'left',
        })

        setTimeout(() => {
            this.setState({

                animation: true,
                anima: false,
                activeStep: this.state.activeStep + 1,
            })
        }, 300);
    };

    handleBack = () => {
        this.setState({
            animation: false,
            anima: true,
            direction: 'right',
        })

        setTimeout(() => {
            this.setState({

                animation: true,
                anima: false,
                activeStep: this.state.activeStep - 1
            })
        }, 300);
    };

    render() {

        const { classes } = this.props;

        if (window.innerWidth > 959) {
            while (categories.length > 0) {
                product.push(categories.splice(0, 4))
            }
        } else if (window.innerWidth < 959 && window.innerWidth > 500) {
            while (categories.length > 0) {
                product.push(categories.splice(0, 2))
            }
        } else if (window.innerWidth <= 500) {
            while (categories.length > 0) {
                product.push(categories.splice(0, 1))
            }
        }

        return (
            <div>
                <Dialog
                    open={this.props.productDetails}
                    TransitionComponent={Transition}
                    fullWidth="true"
                    maxWidth="md"
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"

                >
                    <Grid xs={12} container>
                        <Grid item xs={12} sm={4} md={4} className={classes.fondoDetailProduct}>
                            <Grid container className={classes.paddingDetail}>
                                <Typography variant='h4' className={classes.hiddenMobile}>Mueble relax
                                    <Divider color='primary' className={classes.divider} />
                                </Typography>

                                <Grid item xs={12} sm={12} md={12}
                                    className={classes.hiddenDesktop}>
                                    <Grid container >
                                        < Grid item xs={10} sm={11} md={11}>
                                            <Typography variant='h4'>Mueble relax
                                    <Divider color='primary' className={classes.divider} />
                                            </Typography>
                                        </Grid>
                                        < Grid item xs={2} sm={1} md={1} >
                                            <IconButton position="static" onClick={this.handleClose}>
                                                <i style={{ fontSize: 15, marginTop: -5 }} class="icon-guss_close"></i>
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                                </Grid>

                                <Grid xs={12} md={12} sm={12}>
                                    <Typography variant='h6' color='secondary'>Información del producto</Typography>
                                </Grid>

                                <Grid xs={12} md={12} sm={12}>
                                    <Typography variant='subtitle1' className={classes.margin}>Mueble de la colección hogar.</Typography>
                                </Grid>
                                <Grid xs={12} md={12} sm={12}>
                                    <Typography variant='h6' color='secondary' className={classes.color}>Colores del producto </Typography>
                                </Grid>
                                <Grid container className={classes.margin}>
                                    {colors.map(col => (
                                        <Grid xs={4} sm={3} md={3}>
                                            <Grid container className={classes.color}>
                                                <CardActionArea elevation={3} outlined style={{ backgroundColor: col, height: 45, width: 45 }} >
                                                </CardActionArea >
                                            </Grid>
                                        </Grid>
                                    ))}
                                </Grid>
                            </Grid>

                        </Grid>
                        <Grid item xs={12} sm={8} md={8} className={classes.fondoProduct}>
                            <Grid container>
                                <Grid item xs={3} sm={1} md={1}>
                                    <IconButton position="static" >
                                        <i style={{ fontSize: 15, color: properties.red }} class="icon-guss_like"></i>
                                    </IconButton>
                                </Grid>
                                <Grid item xs={3} sm={1} md={1}>
                                    <IconButton position="static" >
                                        <i style={{ fontSize: 15, color: properties.green }} class="icon-guss_share"></i>
                                    </IconButton>
                                </Grid>
                                <Grid item xs={3} sm={1} md={1}>
                                    <IconButton position="static">
                                        <i style={{ fontSize: 15, color: properties.blue }} class="icon-guss_save"></i>
                                    </IconButton>
                                </Grid>
                                <Grid item xs={3} sm={9} md={9}
                                    container
                                    justify="flex-end" className={classes.hiddenMobile}>
                                    <IconButton position="static" onClick={this.handleClose}>
                                        <i style={{ fontSize: 15 }} class="icon-guss_close"></i>
                                    </IconButton>
                                </Grid>
                                <Grid item xs={12} sm={12} md={12}>
                                    <Magnifier src={SOFA} className={classes.imgDetail} mgBorderWidth={0} mgWidth={300} mgHeight={300} />
                                    <Grid item xs={12} sm={12} md={12}
                                        container
                                        justify="flex-end" className={classes.priceDetail} elevation={0} > 
                                        <strong>Bs. 5.450.00,00</strong>
                                        </Grid>
                                    <Divider />
                                </Grid>
                            </Grid>
                        </Grid>

                    </Grid>
                    <Grid item xs={12} >
                        <Grid container
                            alignItems="center"
                            className={classes.fondoDescription}>

                            <Grid item xs={2} sm={1} md={1}>
                                <Button disabled={this.state.activeStep === 0}>
                                    <ArrowBackIcon className={classes.Arrow} onClick={this.handleBack} />
                                </Button>

                            </Grid>

                            <Grid item xs={8} sm={10} md={10}>
                                <Grow in={this.state.animation}>
                                    <Grid container spacing={0}  >
                                        {product[this.state.activeStep].map(cat => (
                                            <Grid item xs={12} sm={3} md={3}>
                                                <img alt="mueble" src={cat.url} width="160" className={classes.imgDescription} />
                                            </Grid>
                                        ))}

                                    </Grid>
                                </Grow>
                            </Grid>
                            <Grid item xs={2} sm={1} md={1} >
                                <Button disabled={this.state.activeStep === (product.length - 1)}>
                                    <ArrowForwardIcon className={classes.Arrow} onClick={this.handleNext} />
                                </Button>
                            </Grid>

                        </Grid>
                    </Grid>

                    <Grid item xs={12} >
                        <Grid container className={classes.paddingDetail}>
                            <Grid xs={12} md={12} sm={12}>
                                <Typography variant='h5'>Ficha técnica del producto</Typography>
                            </Grid>
                            <Grid xs={12} md={12} sm={12}>
                                <Typography variant='subtitle1' className={classes.margin}>La colección hogar cuenta con una variedad de materiales y colores.</Typography>
                            </Grid>
                            <Grid xs={12} md={12} sm={12}>
                                <Typography variant='subtitle1' className={classes.marginCategory}><strong>Categorías</strong>:</Typography>
                            </Grid>
                            <Grid xs={12} md={12} sm={12} className={classes.margin}>
                                <Chip color={properties.gray} label='Hogar' className={classes.chip} />
                                <Chip color={properties.gray} label='Muebles' className={classes.chip} />
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid item xs={12} >
                        <Grid xs={12} md={12} sm={12} className={classes.answer}>
                            <Typography variant='h5'>Preguntas y respuestas</Typography>
                        </Grid>
                    </Grid>

                    <Grid item xs={12} >
                        <div className={classes.mainData}>
                            <Grid container className={classes.fondoProduct} >
                                <Grid xs={12} md={10} sm={10}>
                                    <Paper component="form" elevation={0}
                                        className={classes.root} >
                                        <InputBase
                                            className={classes.input}
                                            placeholder="Ingrese una nueva pregunta"
                                        />
                                    </Paper>
                                </Grid>
                                <Grid xs={12} md={2} sm={2}>
                                    <Button variant="outlined"
                                        className={classes.button} color="primary" >
                                        {labels.SEND_BUTTOM}
                                    </Button>
                                </Grid>

                                <Grid xs={12} md={12} sm={12}>
                                    <Paper component="form" elevation={0} className={classes.questions}>
                                        <Grid container className={classes.paddingDetail}>
                                            <Grid xs={12} md={12} sm={12} >
                                                <Typography variant='body1' ><strong>María Nieto</strong></Typography>
                                            </Grid>
                                            <Grid xs={12} md={12} sm={12}>
                                                <Typography variant='body2' >24/2/2021</Typography>
                                            </Grid>
                                            <Grid xs={12} md={12} sm={12}>
                                                <Typography variant='body2' className={classes.marginQuestions}>Buenos días, Tienen disponibilidad para 100 unidades?</Typography>
                                            </Grid>
                                        </Grid>
                                        <Grid container className={classes.marginRespuesta}>
                                            <Grid xs={12} md={12} sm={12} >
                                                <Typography variant='body1' color='primary'><strong>Guss Muebles</strong></Typography>
                                            </Grid>
                                            <Grid xs={12} md={12} sm={12}>
                                                <Typography variant='body2' >24/2/2021</Typography>
                                            </Grid>
                                            <Grid xs={12} md={12} sm={12} >
                                                <Typography variant='body2' className={classes.marginQuestions}>Buenos días, disponibilidad total, le invitamos a generar el pedido</Typography>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                            </Grid>
                        </div>
                    </Grid>
                </Dialog>

            </div >
        );
    }
}

const Detail = withStyles(styles, { withTheme: true })(ProductDetail)
function mapStateToProps(state) {
    return {
        productDetails: state.products.productDetails
    }
}

export default connect(
    mapStateToProps, {
    detailProduct
})(Detail);

