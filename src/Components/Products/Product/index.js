import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardActionArea, Divider, CardMedia, Typography, CardActions, Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/core';

/*Components */


/*Actions */
import {detailProduct} from '../../../Redux/Actions/_actionProducts';

/*Constants */
import { properties } from '../../../Constants';
import { styles } from '../constants.js';

import '../index.css'

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }

        this.detailProduct = this.detailProduct.bind(this);
    }

    detailProduct = () => {
        this.props.detailProduct(true);
    }
    
    render() {
        const { producto, classes } = this.props;
        var color = '';
        var top = 0;
        var rayado = '';
        if (producto.subprice !== null) {
            color = properties.red;
            top = 0;
            rayado= 'line-through'
        } else {
            color = properties.secondary
            top = 18;
            rayado= 'none'
        }
        return (
            <Grid container xs={12} md={12}>
                <Card className={classes.card}>
                    <CardActionArea className={classes.CardActionArea}
                    onClick={this.detailProduct}>
                        <CardMedia style={{ height: 10, }} />
                        <img alt="img" src={producto.img} width="250" className={classes.imgCard} />
                    </CardActionArea>
                    <Divider/>
                    <CardActions className={classes.colorCard} >
                        <Grid container>
                            <Grid xs={12} md={12} sm={12}>
                                <Typography color='primary' className={classes.title} >{producto.title}</Typography>
                            </Grid>
                            <Grid xs={12} md={6} sm={6}>
                                <Typography color='secondary' className={classes.subtitle}>{producto.subtitle}</Typography>
                            </Grid>

                            <Grid xs={12} md={6} sm={6}>
                                <Typography variant="h7"  style={{ marginTop: top, float: 'right', fontWeight: 'bold', color: color}} >Bs. {producto.price}</Typography><br/>
                                {/* <Typography variant="h7" className='hiddenMobile' style={{ marginTop: -45, float: 'right', fontWeight: 'bold', color: color }} >Bs. {producto.price}</Typography><br/> */}

                                {producto.subprice !== null &&
                                <>
                                    <Typography color='secondary' style={{ fontSize: 12, float: 'right', textDecoration: rayado  }} >Bs.{producto.subprice}</Typography>
                                    {/* <Typography color='secondary' className='hiddenMobile' style={{ fontSize: 12, float: 'right', marginTop: -45}} >Bs.{producto.subprice}</Typography> */}
                                </>
                                }
                            </Grid>
                        </Grid>
                    </CardActions>
                </Card>
            </Grid >
        );
    }
}

const productData = withStyles(styles, { withTheme: true })(Product)
function mapStateToProps(state) {
    return {
    };
}

export default connect(
    mapStateToProps, {
        detailProduct
})(productData);