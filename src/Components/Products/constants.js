import { properties } from '../../Constants';

export const colors = [
    properties.secondary,
    properties.grayLight,
    properties.white,
    // properties.primary,
    // properties.red
]

export const styles = (theme) => ({
    productImg: {
        height: "10vh",
        objectFit: 'cover',
        width: '100%'
    },
    menuProduct: {
        color: properties.secondary
    },
    paperMenu: {
        paddingLeft: 10,
        height: 740,
        marginTop: 8,
        background: "linear-gradient(to bottom, #c7c7bf 0%,#919799 100%)"
    },
    paperSearch: {
        backgroundColor: properties.transparent,
        height: 140,
        paddingTop: 50,

    },
    search: {
        padding: '0px 30px 0px 30px'
    },
    imgMenu: {
        marginLeft: -100,
        marginTop: -20
    },
    heightProduct: {
        minHeight: 670,
        padding: 10,
        paddingTop: 18
    },
    imgHeight: {
        padding: 8,
    },
    pagination: {
        backgroundColor: properties.grayLight,
        padding: 5,
        marginBottom: 15
    },

    dividerProduct:{
        marginBottom: theme.spacing(13)
    },

    card: {
        width: '100%',
    },

    CardActionArea: {
        height: 220,
        backgroundColor: "white",
    },
    colorCard: {
        backgroundColor: properties.grayLight
    },
    imgCard: {
        width: '100%',
        maxHeight: 200,
        maxWidth: 220,
        display: 'block',
        margin: 'auto'
    },
    title: {
        fontSize: 20
    },
    subtitle: {
        fontSize: 14
    },
    select: {
        backgroundColor: properties.primary,
        padding: 10,
        width: 180,
        color: "white",
        textAlign: "center"
    },
    searchMobile: {
        padding: 28
    },
    modalSearch: {
        backgroundColor: properties.secondary
    },
    modalColor: {
        color: properties.white,
        fontSize: 20
    },
    iconClose: {
        float: "right",
        color: properties.white
    },
    containerSearch: {
        paddingBottom: 25
    },
    subprice: {
        fontSize: 12,
        float: 'right',
        marginTop: -45
    },

    [theme.breakpoints.up('md')]: {
        hiddenDesktop: {
            display: 'none'
        },
    },
    [theme.breakpoints.up('lg')]: {
        hiddenDesktop: {
            display: 'none'
        },
    },
    [theme.breakpoints.up('sm')]: {
        hiddenDesktop: {
            display: 'none'
        },
    },

    [theme.breakpoints.down('xs')]: {
        hiddenMobile: {
            display: 'none'
        },
    },

    fondoDetailProduct: {
        background: properties.gray,
    },
    fondoProduct: {
        background: properties.grayLight,
    },

    paddingDetail: {
        padding: theme.spacing(2),
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(1),
        },
    },
    divider: {
        marginBottom: 20,
    },
    margin: {
        marginLeft: 10,
        [theme.breakpoints.down('xs')]: {
            marginLeft: 0,
        },
    },
    color: {
        marginBottom: 10
    },
    imgDetail: {
        width: '100%',
        maxHeight: 500,
        maxWidth: 450,
        display: 'block',
        margin: 'auto',
        marginTop: -17,
        [theme.breakpoints.down('xs')]: {
            marginTop: -20,
        },
    },

    priceDetail: {
        background: properties.gray,
        opacity: 0.7,
        padding: 10,
        fontSize: 30,
        color: properties.secondary,
        [theme.breakpoints.down('xs')]: {
            fontSize: 20,
        },
    },

    Arrow: {
        color: properties.secondary,
        fontSize: 25,
        margin: 0,
        [theme.breakpoints.down('xs')]: {
            marginRight: 20,
        },
    },
    imgDescription: {
        width: '100%',
        maxHeight: 120,
        maxWidth: 160,
        display: 'block',
        margin: 'auto',
    },

    fondoDescription: {
        background: properties.grayLight,
        height: '100%',
        minHeight: 120,
    },
    marginCategory: {
        marginLeft: 10,
        marginTop: 15,
        [theme.breakpoints.down('xs')]: {
            marginLeft: 0,
        },
    },
    chip: {
        padding: 10,
        marginLeft: 10,
        marginTop: 5,
        border: '1px solid gray',
        [theme.breakpoints.down('xs')]: {
            marginLeft: 5,
        },
    },
    answer: {
        padding: '20px 0px 15px 20px',
        [theme.breakpoints.down('xs')]: {
            padding: '10px 0px 10px 10px'
        },
    },

    root: {
        backgroundColor: properties.white,
        marginLeft: theme.spacing(3),
        marginTop: theme.spacing(2),
        height: 40,
        display: 'flex',
        alignItems: 'center',
        borderRadius: 0,
        border: '1px solid gray',
        [theme.breakpoints.down('xs')]: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
    },

    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },

    button: {
        backgroundColor: properties.white,
        padding: '7px 30px 7px 30px',
        marginLeft: theme.spacing(2),
        marginTop: theme.spacing(2),
        borderRadius: 0,
        [theme.breakpoints.down('xs')]: {
            marginLeft: theme.spacing(1),
            marginTop: theme.spacing(1),
        },

    },

    mainData: {
        height: theme.spacing(32),
        overflow: 'auto',

        '&::-webkit-scrollbar': {
            padding: '0.4em',
            margin: '0.4em',
            width: '0.4em',
            background: '#dfdee3',

        },
        '&::-webkit-scrollbar-track': {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
            webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',

        },
        '&::-webkit-scrollbar-thumb': {
            backgroundColor: '#aadd11',
            // outline: '1px solid slategrey',
            background: '#aadd11',

        }
    },

    questions: {
        marginLeft: theme.spacing(3),
        marginTop: theme.spacing(2),
        marginRight: theme.spacing(3),
        marginBottom: theme.spacing(4),
        borderRadius: 0,
        border: '1px solid gray',
        [theme.breakpoints.down('xs')]: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
    },

    marginQuestions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(2)
    },
    marginRespuesta: {
        paddingRight: theme.spacing(5),
        marginTop: theme.spacing(-5),
        marginLeft: theme.spacing(3),
        textAlign: 'right',
        [theme.breakpoints.down('xs')]: {
            paddingRight: theme.spacing(1),
            marginTop: theme.spacing(-1),
            marginLeft: theme.spacing(1),
            textAlign: 'left',
        },

    }

})
