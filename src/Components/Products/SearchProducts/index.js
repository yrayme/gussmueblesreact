import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Paper, MenuList, MenuItem, Divider, Select, IconButton } from '@material-ui/core';
// import SearchIcon from '@material-ui/icons/Search';
import Pagination from '@material-ui/lab/Pagination';
import { withStyles } from '@material-ui/core';
import SILLA2 from '../../Home/img/silla2.png';

/*Components */
import Search from '../../Home/Search';
import Product from '../Product';
import ProductDetail from '../ProductDetail';

/*Actions */
import { modalSearch } from '../../../Redux/Actions/_actionProducts';

/*Constants */
import { product } from '../../../Constants';

import { styles } from '../constants.js';



class SearchProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeStep: 0,
            producto: [],
            page: 1,
            size: 10
        }
        this.pagination = this.pagination.bind(this);
    }
    componentDidMount() {
        var t2 = []
        var size = 10
        if (window.innerWidth > 959) {
            while (product.length > 0) {
                t2.push(product.splice(0, 8))
            }
        } else if (window.innerWidth < 959 && window.innerWidth > 425) {
            while (product.length > 0) {
                t2.push(product.splice(0, 6))
            }
        } else if (window.innerWidth <= 425) {
            size = 12;
            while (product.length > 0) {
                t2.push(product.splice(0, 3))
            }
        }
        this.setState({
            producto: t2,
            size: size
        })
    }

    pagination(event, value) {
        this.setState({
            activeStep: value - 1,
            page: value
        })
    }

    Search = () => {
        this.props.modalSearch(true);
    }

    render() {
        const { classes } = this.props;

        return (
            <Grid item xs={12} className='d-block w-100 fondo' >
                <Grid container spacing={2}
                    // className={"hiddenMobile hiddenDesktop"} 
                    className={classes.paperSearch}>
                    <Grid item xs={12} className={classes.hiddenMobile} >
                        <Grid container>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={10} className={classes.search}>
                                <Search />
                            </Grid>
                            <Grid item xs={1}>
                            </Grid>

                        </Grid>
                    </Grid>
                    <Grid item xs={12} className={classes.hiddenDesktop} >
                        <Grid container className={classes.searchMobile}>
                            <Grid item xs={10} sm={10}>
                                <Select
                                    className={classes.select}
                                    labelId="demo-simple-select-outlined-label"
                                    id="demo-simple-select-outlined"
                                    value={0}
                                    label="Age"
                                    variant="standard"
                                    color="primary"
                                >
                                    <MenuItem value={0}>Accesorios</MenuItem>
                                    <MenuItem value={1}>Archivadores</MenuItem>
                                    <MenuItem value={2}>Arturitos</MenuItem>
                                    <MenuItem value={3}>Bibliotecas</MenuItem>
                                    <MenuItem value={4}>Escritorios</MenuItem>
                                    <MenuItem value={5}>Mesas</MenuItem>
                                    <MenuItem value={6}>Repuestos</MenuItem>
                                    <MenuItem value={7}>Sillas</MenuItem>
                                    <MenuItem value={8}>Sofás</MenuItem>
                                    <MenuItem value={9}>Tándems</MenuItem>
                                </Select>
                            </Grid>
                            <Grid item xs={2} sm={2}
                                justify="right">
                                <IconButton color="secondary" type="submit" aria-label="search" onClick={this.Search}>
                                    <i style={{ fontSize: 20 }} class="icon-guss_search"></i>
                                </IconButton>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container  >
                    <Grid item xs={12}>
                        <Grid container>
                            <Grid item xs={2} className={classes.hiddenMobile}>
                                <Paper elevation={2} className={classes.paperMenu}>
                                    <MenuList className={classes.menuProduct}>
                                        <MenuItem >Accesorios</MenuItem>
                                        <MenuItem >Archivadores</MenuItem>
                                        <MenuItem >Arturitos</MenuItem>
                                        <MenuItem >Bibliotecas</MenuItem>
                                        <MenuItem >Escritorios</MenuItem>
                                        <MenuItem >Mesas</MenuItem>
                                        <MenuItem >Repuestos</MenuItem>
                                        <MenuItem >Sillas</MenuItem>
                                        <MenuItem >Sofás</MenuItem>
                                        <MenuItem >Tándems</MenuItem>
                                    </MenuList>
                                    <img alt="mueble" src={SILLA2} width="350" height="400" className={classes.imgMenu} />
                                </Paper>
                            </Grid>
                            <Grid item xs={10} className={classes.hiddenMobile} >
                                <Grid container className={classes.heightProduct}>
                                    {
                                        this.state.producto.length > 0 &&
                                        <>
                                            {
                                                this.state.producto[this.state.activeStep].map(prod => (
                                                    <Grid item xs={12} sm={6} md={3} className={classes.imgHeight}>
                                                        <Product
                                                            producto={prod} />
                                                    </Grid>
                                                ))
                                            }
                                        </>
                                    }

                                </Grid>
                                <Grid
                                    container
                                    justify="center"
                                // className={classes.marginPagination}
                                >
                                    <Pagination count={this.state.producto.length}
                                        onChange={this.pagination} shape="rounded" color='primary'
                                        page={this.state.page}
                                        defaultPage={4}
                                        siblingCount={0}
                                        className={classes.pagination} />
                                </Grid>

                            </Grid>
                            <Grid item xs={12} className={classes.hiddenDesktop} >
                                <Grid container className={classes.heightProduct}>
                                    {
                                        this.state.producto.length > 0 &&
                                        <>
                                            {
                                                this.state.producto[this.state.activeStep].map(prod => (
                                                    <Grid item xs={12} sm={6} md={3} className={classes.imgHeight}>
                                                        <Product
                                                            producto={prod} />
                                                    </Grid>
                                                ))
                                            }
                                        </>
                                    }

                                </Grid>
                                <Grid
                                    container
                                    justify="center"
                                // className={classes.marginPagination}
                                >
                                    <Pagination count={this.state.producto.length}
                                        onChange={this.pagination} shape="rounded" color='primary'
                                        page={this.state.page}
                                        defaultPage={4}
                                        siblingCount={0}
                                        className={classes.pagination} />
                                </Grid>

                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Divider className={classes.dividerProduct} />
                {this.props.productDetails && <ProductDetail />}

            </Grid >

        );
    }
}
const products = withStyles(styles, { withTheme: true })(SearchProducts)
function mapStateToProps(state) {
    return {
        productDetails: state.products.productDetails
    };
}

export default connect(
    mapStateToProps, {
    modalSearch
})(products);