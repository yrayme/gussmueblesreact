import { properties } from '../../Constants';
export const styles = () => ({
    googleMap: {
        border: 0,
        width: "100%",
        height: "50vh"
    },
    container: {
        marginTop: -120,
        position: "absolute",

    },
    blueBox: {
        backgroundColor: properties.gray,
        height: 450
    },
    grayBox: {
        backgroundColor: properties.grayLight,
        height: 255,
        marginTop: 200,
        marginLeft: -50,
        borderRadius: 0,
        width: 385
    },

    margenIcons: {
        marginLeft: 1,
        marginRight: -1,
        paddingLeft: 10,
        paddingRight: 30,
        paddingTop: 2

    },
})

export const stylesFooter = (theme) => ({
    paddingFooter: {
        paddingLeft: 40
    },

    [theme.breakpoints.up('md')]: {
        hiddenDesktop: {
            display: 'none'
        },
    },
    [theme.breakpoints.up('lg')]: {
        hiddenDesktop: {
            display: 'none'
        },
    },
    [theme.breakpoints.up('sm')]: {
        hiddenDesktop: {
            display: 'none'
        },
    },

    [theme.breakpoints.down('xs')]: {
        hiddenMobile: {
            display: 'none'
        },
    },
})