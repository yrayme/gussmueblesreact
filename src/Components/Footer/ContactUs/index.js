import React, { Component } from 'react';
import { Grid, withStyles, Paper, TextField, Button, Typography } from '@material-ui/core';
import { Email, Instagram, LocationOnOutlined, MapOutlined, WhatsApp, MailOutlinedIcon, EmailOutlined, ScheduleOutlined } from '@material-ui/icons';



import { styles } from '../constants';

const labels = JSON.parse(localStorage.getItem("labels"))

class Contact extends Component {
    render() {
        const { classes } = this.props;

        return (
            <>
                <Grid container>
                    <Grid item xs={12}>
                        <iframe title="Guss map"
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d980.762189197668!2d-66.87527881490306!3d10.496822783360974!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c2a59771c97c18d%3A0x19be4af332092424!2sGUSS%20Muebles!5e0!3m2!1sen!2sve!4v1613685955540!5m2!1sen!2sve"
                            width="800"
                            height="600"
                            frameborder="0"
                            className={classes.googleMap}
                            allowfullscreen=""
                            aria-hidden="false"
                            tabindex="0"
                        ></iframe>
                    </Grid>
                </Grid>

                <Grid container className={classes.container} >
                    <Grid item xs={0} sm={0} md={3}>
                    </Grid>

                    <Grid item xs={12} sm={6} md={4} className={classes.blueBox}>
                        <Grid container>
                            <Grid item xs={12} style={{ background: "linear-gradient(to left, #013348 0%, #0585ba 100%)", height: 80, color: "white", padding: 20 }}>
                                <Typography variant="h5">_Contáctanos</Typography>

                            </Grid>
                        </Grid>   
                        <Grid container style={{ paddingLeft: 80, paddingRight: 80, paddingTop: 20 }} spacing={2}>
                            <Grid item xs={10}>
                                <TextField
                                    color="secondary"
                                    fullWidth id="standard-basic"
                                    label={labels.EMAIL_LB}
                                />
                            </Grid>
                            <Grid item xs={10}>
                                <TextField
                                    color="secondary"
                                    fullWidth id="standard-basic"
                                    label={labels.NAME_LASTNAME_LB}
                                />
                            </Grid>

                            <Grid item xs={10}> 
                                <TextField
                                    color="secondary"
                                    fullWidth id="standard-basic"
                                    label={labels.COMMENT_LB}
                                    multiline
                                    rows={4}
                                />
                            </Grid>

                            

                            <Grid item xs={12}>
                                <Button size="large" variant="outlined" color="secondary" style={{ borderRadius: 0, marginTop: 20 }}>
                                    {labels.SEND_BUTTOM}
                                </Button>
                            </Grid>

                        </Grid>

                       
                    </Grid>

                        <Grid item xs={12} sm={6} md={3} >
                            <Paper elevation={3} className={classes.grayBox}>
                                <Grid container spacing= {2} className= {classes.margenIcons}>
                                    <Grid item xs={1} >
                                        <Instagram /> 
                                    </Grid> 
                                    <Grid item xs={11} >
                                        <Typography>@gussmuebles</Typography>
                                    </Grid> 

                                    <Grid item xs={1}>
                                        <WhatsApp/>
                                    </Grid> 
                                    
                                    <Grid item xs={11}>
                                        <Typography>+58 4242540002</Typography>
                                    </Grid> 

                                    <Grid item xs={1} >
                                        <LocationOnOutlined/>
                                    </Grid> 

                                    <Grid item xs={11}>
                                        <Typography>guss.contacto@gmail.com</Typography>
                                    </Grid>

                                    <Grid item xs={1}  >
                                        <EmailOutlined/>
                                    </Grid> 

                                    <Grid item xs={11}>
                                        <Typography>Horario de atención - Lunes a Viernes de 8:00 AM a 5:30 PM</Typography>
                                    </Grid> 

                                    <Grid item xs={1}>
                                        <ScheduleOutlined/>
                                    </Grid> 
                                    
                                    <Grid item xs={11}>
                                        <Typography>Edificio Majestic, Av. Libertador, Venezuela, Distrito capital</Typography>
                                    </Grid> 

                                </Grid>
                            
                            </Paper> 
                        </Grid>   

                    <Grid item xs={0} sm={0} md={2}>
                    </Grid>
                </Grid>
            </>
        );
    }
}

const ContactUs = withStyles(styles, { withTheme: true })(Contact)

export default ContactUs;