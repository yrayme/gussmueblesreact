import React, { Component } from 'react';
import { Grid, Divider } from '@material-ui/core';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';

import LOGO from '../Home/img/gusslogo.png';
import FACEBOOK from './img/facebook.png';
import INSTA from './img/instagram.png';
import TWITTER from './img/twitter.png';

/*Constants */
import { stylesFooter } from './constants.js';


class Footer extends Component {
    componentDidMount() {

    }
    render() {

        const { classes } = this.props;

        return (
            <Grid container>
                <Grid item xs={12}>
                    <Grid container className={classes.hiddenMobile}>
                        <Grid item xs={4}>

                        </Grid>
                        <Grid item xs={4} >
                            <Grid container>
                                <Grid item xs={3}>

                                </Grid>
                                <Grid item xs={6} >
                                    <Grid container>
                                        <Grid item xs={6}>
                                            <img width={100} alt="logo" src={LOGO} />
                                        </Grid>
                                        <Grid item xs={2} >
                                            <img width={25} alt="logo" src={FACEBOOK} />
                                        </Grid>
                                        <Grid item xs={2} >
                                            <img width={25} alt="logo" src={INSTA} />
                                        </Grid>
                                        <Grid item xs={2}>
                                            <img width={25} alt="logo" src={TWITTER} />
                                        </Grid>
                                    </Grid>

                                </Grid>
                                <Grid item xs={3}>


                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={4}>

                        </Grid>
                    </Grid>

                    <Grid container className={classes.hiddenDesktop}>
                        <Grid item xs={12} >
                            <Grid container className={classes.paddingFooter}>
                                <Grid item xs={6}>
                                    <img width={100} alt="logo" src={LOGO} />
                                </Grid>
                                <Grid item xs={2} >
                                    <img width={25} alt="logo" src={FACEBOOK} />
                                </Grid>
                                <Grid item xs={2} >
                                    <img width={25} alt="logo" src={INSTA} />
                                </Grid>
                                <Grid item xs={2}>
                                    <img width={25} alt="logo" src={TWITTER} />
                                </Grid>
                            </Grid>

                        </Grid>

                    </Grid>
                </Grid>
                <Grid item xs={4}>
                </Grid>
                <Grid item xs={4} style={{ marginTop: 10, marginBottom: 20 }}>
                    <Divider></Divider>
                </Grid>
                <Grid item xs={4}>
                </Grid>
                <Grid item xs={12} style={{ marginTop: 25, marginBottom: 25, textAlign: "center" }}>
                    <Grid container className={classes.hiddenMobile}>
                        <Grid item xs={2}>

                        </Grid>

                        <Grid item xs={8}>
                            <Grid container>
                                <Grid item xs={3}>
                                    Nosotros
                                </Grid>
                                <Grid item xs={3}>
                                    Blog
                                </Grid>
                                <Grid item xs={3}>
                                    Tips
                                </Grid>
                                <Grid item xs={3}>
                                    Contáctanos
                                </Grid>
                            </Grid>
                        </Grid>

                        <Grid item xs={2}>

                        </Grid>
                    </Grid>
                    <Grid container className={classes.hiddenDesktop}>
                        <Grid item xs={12}>
                            <Grid container>
                                <Grid item xs={4}>
                                    Nosotros
                                </Grid>
                                <Grid item xs={2}>
                                    Blog
                                </Grid>
                                <Grid item xs={2}>
                                    Tips
                                </Grid>
                                <Grid item xs={4}>
                                    Contactanos
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

const footers = withStyles(stylesFooter, { withTheme: true })(Footer)

function mapStateToProps(state) {
    return {
    };
}

export default connect(
    mapStateToProps, {
})(footers);