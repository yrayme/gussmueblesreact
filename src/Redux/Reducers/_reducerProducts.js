import { OPEN_PRODUCTS, MODAL_SEARCH, DETAIL_PRODUCT } from '../constants';

export const products = (state = {}, action) => {
    switch (action.type) {
        case OPEN_PRODUCTS:
            return { ...state, productOpen: action.payload }
        case MODAL_SEARCH:
            return { ...state, searchModal: action.payload }
        case DETAIL_PRODUCT:
            return { ...state, productDetails: action.payload }

        default:
            return state;
    }
}