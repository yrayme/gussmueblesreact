import { combineReducers } from 'redux';
import { RESET_ALL } from '../constants';
import { products } from './_reducerProducts';
import { headerProps } from './_reducerHeader';

const appReducer = combineReducers({
    products,
    headerProps,
});

const resetState = combineReducers({
    products,
    headerProps,
});

const rootReducer = (state, action) => {
    if (action.type === RESET_ALL) {
        state = undefined
        return resetState(state, action)
    } else {
        return appReducer(state, action)
    }
}

export default rootReducer;