import {OPEN_PRODUCTS, MODAL_SEARCH, DETAIL_PRODUCT} from '../constants';

export const openProduct = (payload) => {
    return {
        type: OPEN_PRODUCTS,
        payload: payload
    }
}

export const modalSearch = (payload) => {
    return {
        type: MODAL_SEARCH,
        payload: payload
    }
}

export const detailProduct = (payload) => {
    return {
        type: DETAIL_PRODUCT,
        payload: payload
    }
}