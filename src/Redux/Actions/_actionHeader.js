import { SET_HEADER_PROPS } from '../constants';

export const setHeaderProps = (headerColor, iconsColor) => {
    return {
        type: SET_HEADER_PROPS,
        iconsColor: iconsColor,
        headerColor: headerColor,

    }
}